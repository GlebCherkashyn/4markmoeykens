//
//  ViewController.swift
//  AnimatorTechnic
//
//  Created by Gleb Cherkashyn on 07.04.2018.
//  Copyright © 2018 RevolverStudio. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {

    //MARK: Outlets
    @IBOutlet var animator: MainScreenAnimator!
    
    //MARK: Life cycle
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        animator.showSplashView()
    }
}

