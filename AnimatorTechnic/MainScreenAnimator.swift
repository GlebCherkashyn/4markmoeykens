//
//  MainScreenAnimator.swift
//  AnimatorTechnic
//
//  Created by Gleb Cherkashyn on 07.04.2018.
//  Copyright © 2018 RevolverStudio. All rights reserved.
//

import UIKit

fileprivate enum Direction {
    case right
    case left
    case up
    case down
}

class MainScreenAnimator: NSObject {

    //MARK: Outlets
    /**
     The cool thing is that you can hold outlets here! Somethimes you need them in view controllers only to move, hide, resize etc. With animator VC even doesn't know about UI elements :) Just use api like
     
        animator.showSomeView()
        animator.showMenu(true)
     
     Btw some actions (toggles usually) can be handled here as well
     
     @IBAction func toggleMenu(_ sender: UIButton) {
     
     }
     
     */
    
    
    // It's often useful to have outlet for VC's view to know it's frame for different devices or orientations
    @IBOutlet weak var mainScreenView: UIView!
    @IBOutlet weak var splashView: UIView!
    
    @IBOutlet weak var simpleLabel: UILabel!
    @IBOutlet weak var splashLabel: UILabel!
    @IBOutlet weak var screenLabel: UILabel!
    @IBOutlet weak var smileLabel: UILabel!
    // I use containers to animate group of elements
    @IBOutlet weak var allElementsContainer: UIView!
    @IBOutlet weak var wordsContainer: UIView!
    
    // MARK: Public methods
    func showSplashView() {
        setupElements()
        runAnimation()
    }
    
    // MARK: Internal
    fileprivate func setupElements() {
        
        splashView.bounds = mainScreenView.bounds
        mainScreenView.addSubview(splashView)
        splashView.center = mainScreenView.center
        allElementsContainer.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
        allElementsContainer.center = splashView.center
        hide(simpleLabel, direction: .left)
        hide(splashLabel, direction: .right)
        hide(screenLabel, direction: .down)
        hide(smileLabel, direction: .down)
    }
    
    fileprivate func runAnimation() {
        
        self.show(self.simpleLabel, delay: 0.2)
        self.show(self.splashLabel, delay: 0.35)
        self.show(self.screenLabel, delay: 0.50) { success in
            
            self.showSmile()
        }
    }
    
    fileprivate func showSmile() {
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            
            let translation = CGAffineTransform(translationX: 0,
                                                y: -(self.mainScreenView.frame.height * 0.2))
            let scale = CGAffineTransform(scaleX: 0.85, y: 0.85)
            
            self.wordsContainer.transform = translation.concatenating(scale)
            
            self.show(self.smileLabel, delay: 0.0)
            
        }) { (success) in
            self.hideSplashView()
        }
    }
    
    fileprivate func hideSplashView() {
        UIView.animate(withDuration: 0.2, delay: 0.3, options: .curveEaseOut, animations: {
            self.allElementsContainer.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
        }) {(_) in
            UIView.animate(withDuration: 0.4, delay: 0, options: .curveEaseIn, animations: {
                self.splashView.alpha = 0.0
                self.allElementsContainer.transform = CGAffineTransform(scaleX: 3.0, y: 3.0)
            }) { (success) in
                
                // We don't need it anymore. Also helps to avoid rare bugs in different orientations
                self.splashView.removeFromSuperview()
            }
        }
    }
}

// MARK: Helpers
extension MainScreenAnimator {
    
    // Something like 'AnimatorHelpersX' =)
    fileprivate func hide(_ view: UIView, direction: Direction) {
        
        view.alpha = 0.0
        
        var delta = (x: CGFloat(0.0), y: CGFloat(0.0))
        
        let convertedRect = mainScreenView.convert(view.frame, to: mainScreenView)
        
        switch direction {
        case .right:
            delta.x = mainScreenView.frame.width - convertedRect.origin.x
        case .left:
            delta.x = -convertedRect.origin.x - view.frame.width
        case .up:
            delta.y = -convertedRect.origin.y - view.frame.height
        case .down:
            delta.y = mainScreenView.frame.height - convertedRect.origin.y
        }
        
        view.transform = CGAffineTransform(translationX: delta.x, y: delta.y)
    }
    
    fileprivate func show(_ view: UIView,
                          delay: TimeInterval,
                          completion: ((Bool) -> Void)? = nil) {
        
        let interval = 0.4 + delay
        
        UIView.animate(withDuration: interval, delay: delay, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            
            view.transform = .identity
            view.alpha = 1.0
            
        }) { (success) in
            if let completion = completion {
                completion(success)
            }
        }
    }
}
